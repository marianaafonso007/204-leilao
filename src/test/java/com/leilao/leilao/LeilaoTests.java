package com.leilao.leilao;

import com.leilao.leilao.models.Lance;
import com.leilao.leilao.models.Usuario;
import com.leilao.leilao.services.LeilaoService;
import jdk.nashorn.internal.objects.NativeUint8Array;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.management.InvalidAttributeValueException;
import java.util.ArrayList;
import java.util.List;

public class LeilaoTests {
    @Test
    public void testarAdicionarLanceSucesso(){
        Usuario usuario = new Usuario(1, "Mariana Felix");
        Lance lance = new Lance(usuario, 500);
        List<Lance> lances = new ArrayList<>();
        LeilaoService leilaoService = new LeilaoService(lances);
        Assertions.assertTrue(leilaoService.AdicionarLance(lance));
    }

    @Test
    public void testarLanceMaiorQueAnteriorFalha(){
        Usuario usuario = new Usuario(1, "Mariana Felix");
        Lance lance = new Lance(usuario, 0);
        List<Lance> lances =  new ArrayList<>();
        lances.add(lance);
        LeilaoService leilaoService = new LeilaoService(lances);
        Assertions.assertFalse(leilaoService.ValidarLance(lance));
    }

    @Test
    public void testarLanceMaiorQueAnteriorSucesso(){
        Usuario usuario = new Usuario(1, "Mariana Felix");
        Lance lance = new Lance(usuario, 500);
        Lance lance2 = new Lance(usuario, 400);
        Lance lance3 = new Lance(usuario, 450);
        List<Lance> lances =  new ArrayList<>();
        lances.add(lance2);
        lances.add(lance3);
        LeilaoService leilaoService = new LeilaoService(lances);
        Assertions.assertTrue(leilaoService.ValidarLance(lance));
    }

    @Test
    public void testarLanceMaiorQueAnteriorFalse(){
        Usuario usuario = new Usuario(1, "Mariana Felix");
        Lance lance = new Lance(usuario, 100);
        Lance lance2 = new Lance(usuario, 400);
        Lance lance3 = new Lance(usuario, 450);
        List<Lance> lances =  new ArrayList<>();
        lances.add(lance2);
        lances.add(lance3);
        LeilaoService leilaoService = new LeilaoService(lances);
        Assertions.assertFalse(leilaoService.ValidarLance(lance));
    }

    @Test
    public void testarRetornarMaiorLaSucesso(){
        Usuario usuario = new Usuario(1, "Mariana Felix");
        Lance lance = new Lance(usuario, 100);
        Lance lance2 = new Lance(usuario, 400);
        Lance lance3 = new Lance(usuario, 450);
        List<Lance> lances =  new ArrayList<>();
        lances.add(lance);
        lances.add(lance2);
        lances.add(lance3);
        LeilaoService leilaoService = new LeilaoService(lances);
        Lance maiorLance = leilaoService.RetornarMaiorLance();
        boolean validacao = true;
        if (maiorLance.getUsuario() != lance3.getUsuario() || maiorLance.getValorDoLance() != lance3.getValorDoLance()){
            validacao = false;
        }
        Assertions.assertTrue(validacao);
    }

    @Test
    public void testarRetornarMaiorLaErro(){
        Usuario usuario = new Usuario(1, "Mariana Felix");
        Lance lance = new Lance(usuario, 100);
        Lance lance2 = new Lance(usuario, 400);
        Lance lance3 = new Lance(usuario, 450);
        List<Lance> lances =  new ArrayList<>();
        lances.add(lance);
        lances.add(lance2);
        lances.add(lance3);
        LeilaoService leilaoService = new LeilaoService(lances);
        Lance maiorLance = leilaoService.RetornarMaiorLance();
        boolean validacao = true;
        if (maiorLance.getUsuario() != lance2.getUsuario() || maiorLance.getValorDoLance() != lance2.getValorDoLance()){
            validacao = false;
        }
        Assertions.assertFalse(validacao);
    }

    //InvalidAttributeValueException
    @Test
    public void testarRetornarMaiorLaErro2() {
        LeilaoService leilaoService = new LeilaoService();
        Assertions.assertThrows(NullPointerException.class, () -> {leilaoService.RetornarMaiorLance();});
    }

}
