package com.leilao.leilao.services;

import com.leilao.leilao.models.Lance;
import com.leilao.leilao.models.Leilao;

import java.util.ArrayList;
import java.util.List;

public class LeilaoService {
    private static Leilao leilaos;

    public LeilaoService() {
    }

    public LeilaoService(List<Lance> lances) {
        leilaos = new Leilao();
        leilaos.setlances(lances);
    }

    public static boolean AdicionarLance(Lance lance){
            List<Lance> lances = new ArrayList<>();
            lances.add(lance);
            leilaos.setlances(lances);
            return true;
     }

     public static boolean ValidarLance(Lance lance){
        double maiorLance = 0;
         if (lance.getValorDoLance() == 0){
             return false;
         }
        for (Lance lanceMaior : leilaos.getlances()){
            if(lanceMaior.getValorDoLance() > maiorLance){
                maiorLance = lanceMaior.getValorDoLance();
            }
        }

        if(lance.getValorDoLance() < maiorLance ){
            return false;
        }else {
            return true;
        }
     }

     public static Lance RetornarMaiorLance(){
        try {
            Lance lanceMaior = new Lance();
            double valorLanceMaior = 0;
            for (Lance lance : leilaos.getlances()){
                if(lance.getValorDoLance() > valorLanceMaior){
                    valorLanceMaior = lance.getValorDoLance();
                    lanceMaior.setValorDoLance(lance.getValorDoLance());
                    lanceMaior.setUsuario(lance.getUsuario());
                }
            }
            return lanceMaior;
        }catch (Exception e){
            throw new NullPointerException();
        }
     }
}

