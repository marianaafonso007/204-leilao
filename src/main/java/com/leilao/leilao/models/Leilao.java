package com.leilao.leilao.models;

import java.util.ArrayList;
import java.util.List;

public class Leilao {

    private List<Lance> lances;

    public Leilao() {
    }

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public List<Lance> getlances() {
        return lances;
    }

    public void setlances(List<Lance> lances) {
        this.lances = lances;
    }

}